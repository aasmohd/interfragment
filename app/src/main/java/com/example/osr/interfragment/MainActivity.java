package com.example.osr.interfragment;




import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements Communicator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public void respond(String data) {

        FragmentManager mg=getSupportFragmentManager();

          FragmentB f2 =(FragmentB) mg.findFragmentById(R.id.f2);
          f2.changeText(data);
    }


}
