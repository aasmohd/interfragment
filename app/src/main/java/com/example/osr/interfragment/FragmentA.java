package com.example.osr.interfragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class FragmentA extends Fragment// implements View.OnClickListener
{

    Button button;
    int Counter=0;
    Communicator comm;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_a, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        comm=(Communicator) getActivity();

        button=(Button) getActivity().findViewById(R.id.b1);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Counter++;
       comm.respond("the button clicked"+Counter+" times");
            }
        });
      //  button.setOnClickListener(this);   I MEAN NO NEED TO IMPLEMENT ONCLICK LISTENER;
    }

//    @Override
//    public void onClick(View v) {
//        Counter++;
//        comm.respond("the button clicked"+Counter+" times");
//
//    }
}
