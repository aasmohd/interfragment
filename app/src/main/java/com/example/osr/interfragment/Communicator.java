package com.example.osr.interfragment;

/**
 * Created by H.P on 11/18/2017.
 */

public interface Communicator {
    public void respond(String data);
}
